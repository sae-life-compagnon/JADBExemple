import se.vidstige.jadb.JadbConnection;
import se.vidstige.jadb.JadbDevice;
import se.vidstige.jadb.JadbException;

import java.io.*;


public class ScriptMyApp {
    public static void main(String[] args) {
        try {
            startADBServer();
            String serialNumber = getDevice();
            startApp(serialNumber);

            // callNumber(serialNumber, "null", "null", "true"); // hangUp
            // callNumber(serialNumber, "null", "true", "null"); // speaker
            // callNumber(serialNumber, "0782239208", "true", "null"); // call

            // StatusSMSUpdate(serialNumber, "22526"); // StatusSMSUpdate

            // startSmsReaderService(serialNumber, null, 1, 0, 10); //readConversation
            // startSmsReaderService(serialNumber, "0782239208", 3, null, null); //readSms

            // sendSMS(serialNumber, "0782239208", "Ceci\\ est\\ un\\ exemple\\ de\\ SMS\\ envoy\u00e9\\ via\\ JADB."); // sendSMS

        } catch (IOException | JadbException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void callNumber(String serialNumber, String phoneNumber, String speaker, String hangUp) throws IOException {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder("adb", "-s", serialNumber, "shell", "am", "startservice", "-n", "org.lifecompanion/.CallService", "--es", "phoneNumber", phoneNumber, "--es", "speaker", String.valueOf(speaker), "--es", "hangUp", String.valueOf(hangUp));

            Process startServiceProcess = processBuilder.start();
            startServiceProcess.waitFor();

        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void StatusSMSUpdate (String serialNumber, String messageId) throws IOException {
        try {
            String startServiceCmd = String.format("adb -s " + serialNumber + " shell am startservice -n org.lifecompanion/.SMSServiceReader --es messageId %s", messageId);
            Process startServiceProcess = Runtime.getRuntime().exec(startServiceCmd);
            startServiceProcess.waitFor();

        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void startSmsReaderService(String serialNumber, String phoneNumber, int numberOfMessages, Integer startId, Integer endId) {
        try {
            String startServiceCmd = String.format("adb -s %s shell am startservice -n org.lifecompanion/.SMSServiceReader --es phoneNumber %s --ei numberOfMessages %d --ei start %d --ei end %d", serialNumber, phoneNumber, numberOfMessages, startId, endId);

            ProcessBuilder processBuilder = new ProcessBuilder(startServiceCmd.split(" "));
            Process startServiceProcess = processBuilder.start();

            startServiceProcess.waitFor();

        } catch (IOException | InterruptedException e) {
            System.out.printf("Erreur lors du lancement du service ou de la lecture des logs : %s", e.getMessage());
        }
    }
    public static void sendSMS(String serialNumber, String phoneNumber, String message) throws IOException {
        try {
            String startServiceCmd = String.format("adb -s " + serialNumber + " shell am startservice -n org.lifecompanion/org.lifecompanion.SMSServiceSend --es phoneNumber %s --es message %s", phoneNumber, message);
            Process startServiceProcess = Runtime.getRuntime().exec(startServiceCmd);
            startServiceProcess.waitFor();

        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    // Méthode pour le bon fonctionnement de l'application -- A ne pas toucher
    public static void startADBServer() throws IOException, InterruptedException {
        System.out.println("StartADBServer");
        Process process = Runtime.getRuntime().exec("adb start-server");
        process.waitFor();
    }
    public static void closeADBServer() throws IOException, InterruptedException {
        System.out.println("CloseADBServer");
        Process process = Runtime.getRuntime().exec("adb kill-server");
        process.waitFor();
    }
    public static String getDevice() throws IOException, InterruptedException, JadbException {
        JadbConnection jadb = new JadbConnection();
        JadbDevice device = jadb.getDevices().get(0);
        if (device == null) {
            System.out.println("Aucun appareil connecté");
        } else {
            System.out.println("Appareil connecté : " + device.getSerial());
        }
        return device.getSerial();
    }
    private static void startApp(String serialNumber) throws IOException, InterruptedException {
        if (!isAppRunning(serialNumber)) {
            String startServiceCmd = String.format("adb -s %s shell am start -n com.example.myapplication/com.example.myapplication.MainActivity", serialNumber);
            Process startServiceProcess = Runtime.getRuntime().exec(startServiceCmd);
            startServiceProcess.waitFor();
        }
    }
    private static boolean isAppRunning(String serialNumber) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder("adb", "-s", serialNumber, "shell", "pidof", "com.example.myapplication");
        Process process = processBuilder.start();
        int exitCode = process.waitFor();
        return exitCode != 0;
    }
}